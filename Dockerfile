FROM node:10-buster

USER 0:0
RUN groupadd -r -g 999 docker
RUN useradd -m -d /home/node/app -u 999 -g docker -s /bin/bash -r docker
RUN npm install serve -g
RUN mkdir -p ./home/node/app/
ADD / ./home/node/app/
ADD start.sh /
RUN chown -R 999.999 ./home/node

USER 999:999
RUN cd ./home/node/app && npm install && npm run build

EXPOSE 8080

ENTRYPOINT ["/start.sh"]

